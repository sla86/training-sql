package sql.util;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;

import sql.domain.Question;

public class QuestionsLoader {

    private static final ObjectMapper mapper = new ObjectMapper();

    public static List<Question> loadQuestions(final String fileName) {
        List<Question> questions = new ArrayList<>();
        try {
            final URL path = QuestionsLoader.class.getResource("/" + fileName);
            final JsonNode jsonNode = mapper.readTree(path);
            final JsonNode arrayNode = extractArrayNode(jsonNode);
            for (final JsonNode anArrayNode : arrayNode) {
                questions.add(extractQuestion(anArrayNode));
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return questions;
    }

    private static ArrayNode extractArrayNode(final JsonNode jsonNode) {
        if (!jsonNode.isArray()) {
            throw new IllegalArgumentException("Wrong json");
        }
        return (ArrayNode) jsonNode;
    }

    private static Question extractQuestion(final JsonNode jsonNode) {
        final int id = jsonNode.get("id").asInt();
        final String question = jsonNode.get("question").asText();
        final String resultMD5 = Optional.ofNullable(jsonNode.get("resultMD5")).map(JsonNode::asText).orElse(null);
        final String answer = jsonNode.get("answer").asText();
        return new Question(id, question, resultMD5, answer);
    }

}
