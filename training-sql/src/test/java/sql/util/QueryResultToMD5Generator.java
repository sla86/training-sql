package sql.util;

import java.io.File;
import java.io.FileWriter;
import java.nio.charset.Charset;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

import javax.xml.bind.DatatypeConverter;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.util.DigestUtils;

import sql.domain.Question;

@SpringBootTest
class QueryResultToMD5Generator {

    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Test
    void generateWithoutAnswers() {
        generateIt(q -> new Question(q.getId(), q.getQuestion(), getMD5(jdbcTemplate.queryForList(q.getAnswer())), null));
    }

    @Test
    void generateWithAnswers() {
        generateIt(q -> new Question(q.getId(), q.getQuestion(), getMD5(jdbcTemplate.queryForList(q.getAnswer())), q.getAnswer()));
    }

    private void generateIt(Function<Question, Question> consumer) {
        final List<Question> questions = QuestionsLoader.loadQuestions("questions-in.json");
        final String questionsAsJson = "[" + questions.stream()
                .map(consumer)
                .map(Question::toJson)
                .collect(Collectors.joining(",")) + "]";
        writeToFile(questionsAsJson);
    }

    private void writeToFile(final String questionsAsJson) {
        try {
            String filePath = String.format(".%ssrc%stest%sresources%squestions-out.json", File.separator, File.separator, File.separator, File.separator);
            final FileWriter fileWriter = new FileWriter(filePath);
            fileWriter.write(questionsAsJson);
            fileWriter.close();
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    private String getMD5(final List<Map<String, Object>> resultList) {
        final byte[] bytes = DigestUtils.md5Digest(asString(resultList).getBytes(Charset.forName("UTF-8")));
        return DatatypeConverter.printHexBinary(bytes);
    }

    private String asString(final List<Map<String, Object>> resultList) {
        return resultList.stream()
                .map(Object::toString)
                .sorted()
                .collect(Collectors.joining());
    }
}
