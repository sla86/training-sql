package sql.util;

import java.nio.charset.Charset;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.xml.bind.DatatypeConverter;

import org.springframework.util.DigestUtils;

public class QueryResultToMD5Converter {

    public static String getMD5(final List<Map<String, Object>> resultList) {
        final byte[] bytes = DigestUtils.md5Digest(asString(resultList).getBytes(Charset.forName("UTF-8")));
        return DatatypeConverter.printHexBinary(bytes);
    }

    private static String asString(final List<Map<String, Object>> resultList) {
        return resultList.stream()
                .map(Object::toString)
                .sorted()
                .collect(Collectors.joining());
    }
}
