package sql.domain;

import java.util.Optional;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.ToString;

@AllArgsConstructor
@ToString(of = {"id"})
@Getter
public class Question {

    private final Integer id;

    private final String question;

    private final String resultMD5;

    private final String answer;

    public String toJson() {
        return String.format(
                "{"
                        + "\"id\": \"%s\","
                        + "\"question\": \"%s\","
                        + "\"resultMD5\": \"%s\","
                        + "\"answer\":\"%s\""
                        + "}",
                id, question, Optional.ofNullable(resultMD5).orElse(""), Optional.ofNullable(answer).orElse("")
        );

    }
}
