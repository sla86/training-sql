package sql.test;

import java.util.List;
import java.util.Map;
import java.util.stream.Stream;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.jdbc.core.JdbcTemplate;

import sql.domain.Question;
import sql.util.QueryResultToMD5Converter;
import sql.util.QuestionsLoader;

@SpringBootTest
class TestSql {

    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Test
    void testDbConnection() {
        final String result = jdbcTemplate.queryForObject("select * from dual", String.class);
        Assertions.assertEquals("X", result);
    }

    @ParameterizedTest
    @MethodSource("loadQuestion")
    void testSqlQuestions(Question question) {
        final List<Map<String, Object>> resultList = jdbcTemplate.queryForList(question.getAnswer());
        final String md5 = QueryResultToMD5Converter.getMD5(resultList);
        Assertions.assertEquals(question.getResultMD5(), md5);
    }

    private static Stream<Question> loadQuestion() {
        return QuestionsLoader.loadQuestions("questions-out.json").stream();
    }

}
